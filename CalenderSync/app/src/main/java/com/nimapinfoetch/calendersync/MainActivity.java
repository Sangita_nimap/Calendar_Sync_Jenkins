package com.nimapinfoetch.calendersync;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    EditText eventTitle, eventDescription, eventStartDate;

    Button addEvent,deleteEvent, editEvent;

    private final int MY_PERMISSIONS_REQUEST_READ_CALENDAR = 123;

    private static final int DATE_DIALOG_ID = 10;
    private static final int TIME_DIALOG_ID = 20;

    //Related to Calendar
    Date date;
    private int year;
    private int month;
    private int day;
    private int mhour;
    private int mminute;
    public int selectedYear;
    public int selectedMonth;
    public int selectedDay;

    long startDateInlong = 0;
    int eventId = 0;

    Uri eventUri,reminderUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadView();
        initializeCalendarData();
        setUri();
    }

    private void loadView(){
        addEvent  = (Button)findViewById(R.id.addEvent);
        editEvent  = (Button)findViewById(R.id.editEvent);
        deleteEvent = (Button)findViewById(R.id.deleteEvent);

        eventTitle = (EditText) findViewById(R.id.eventTitle);
        eventDescription= (EditText) findViewById(R.id.eventDescription);
        eventStartDate = (EditText) findViewById(R.id.eventStartDate);

        addEvent.setOnClickListener(this);
        editEvent.setOnClickListener(this);
        deleteEvent.setOnClickListener(this);
        eventStartDate.setOnTouchListener(this);
    }

    private void initializeCalendarData() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        mhour = c.get(Calendar.HOUR_OF_DAY);
        mminute = c.get(Calendar.MINUTE);
        date = new Date();
        c.set(year, month, day, 0, 0);
        date.setTime(c.getTimeInMillis());
    }

    private void setUri(){
        if (android.os.Build.VERSION.SDK_INT <= 7) {
            // the old way
            eventUri = Uri.parse("content://calendar/events");
            reminderUri = Uri.parse("content://calendar/reminders");
        } else {
            // the new way
            eventUri = Uri.parse("content://com.android.calendar/events");
            reminderUri = Uri.parse("content://com.android.calendar/reminders");
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.addEvent){
            EventCalender eventCalender = new EventCalender();
            eventCalender.execute();
        }
        else if(v.getId() == R.id.editEvent){
            editEventToCalender(eventId);
        }
        else if(v.getId() == R.id.deleteEvent){
            deleteEventToCalender(eventId);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.eventStartDate) {
            showDialog(DATE_DIALOG_ID);
            return true;
        }
        return false;
    }

    //Local Methods
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateSetListener, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(date.getTime());
                return datePickerDialog;

            case TIME_DIALOG_ID:
                return new TimePickerDialog(this, mTimeSetListener, mhour, mminute, false);

        }
        return null;
    }

    //All Dialog methods
    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            selectedYear = i;
            selectedMonth = i1;
            selectedDay = i2;

            showDialog(TIME_DIALOG_ID);
        }
    };
    long startDate = 0;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                    updateTimeFrom(hourOfDay,minute);
                }
            };



    private void updateTimeFrom(int hours, int mins) {
        String timeSet = "";
        int ampm = 0;
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
            ampm = 1;
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
            ampm = 0;
        } else if (hours == 12) {
            timeSet = "PM";
            ampm = 1;
        }
        else{
            timeSet = "AM";
            ampm = 0;
        }

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);


        String toParse = ""+selectedDay+"-"+(selectedMonth+1)+"-"+selectedYear+" "+hours+":"+mins+" "+timeSet; // Results in "2-5-2012 20:43"
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm aa"); // I assume d-M, you may refer to M-d for month-day instead.

        try {
            Date date1 = formatter.parse(toParse);

            startDateInlong = date1.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String aTime = new StringBuilder().append((selectedMonth + 1) + "/" + selectedDay + "/" + selectedYear).append(" ").append(hours).append(':').append(minutes).append(" ").append(timeSet).toString();

        eventStartDate.setText(aTime);
    }



    private int deleteEventToCalender(int eventId) {

        Uri deleteeventUri = ContentUris
                .withAppendedId(eventUri, eventId);
        eventId = getContentResolver().delete(deleteeventUri, null, null);

        return eventId;

    }

    private int editEventToCalender(int entryID) {
        int iNumRowsUpdated = 0;

        ContentValues event = new ContentValues();

        event.put(CalendarContract.Events.TITLE, eventTitle.getText().toString().trim());
        event.put(CalendarContract.Events.DESCRIPTION,eventDescription.getText().toString().trim()); // 0 for false, 1 for true

        long endDate = startDateInlong + 1000 * 60 * 60;

        event.put(CalendarContract.Events.DTSTART, startDateInlong);
        event.put(CalendarContract.Events.DTEND, endDate);

//        Uri eventsUri = eventUri;
        Uri eventUpdateUri = ContentUris.withAppendedId(eventUri, entryID);

        iNumRowsUpdated = getContentResolver().update(eventUpdateUri, event, null,
                null);

        return iNumRowsUpdated;
    }

    public int addEventToCalender() {

        boolean isRemind = true;

        ContentValues event = new ContentValues();
        // id, We need to choose from our mobile for primary its 1
        event.put(CalendarContract.Events.CALENDAR_ID,1);
        event.put(CalendarContract.Events.TITLE, eventTitle.getText().toString().trim());
        event.put(CalendarContract.Events.DESCRIPTION, eventDescription.getText().toString().trim());
        event.put(CalendarContract.Events.EVENT_LOCATION, "mumbai");

        TimeZone timeZone = TimeZone.getDefault();
        event.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());

        // For next 1hr
        long endDate = startDateInlong + 1000 * 60 * 60;
        event.put(CalendarContract.Events.DTSTART, startDateInlong);
        event.put(CalendarContract.Events.DTEND, endDate);
        event.put(CalendarContract.Events.STATUS, "status");
        event.put(CalendarContract.Events.HAS_ALARM, 1);

        int eventID = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkPermission()) {
                Uri eventUriForId = getContentResolver().insert(eventUri, event);
                eventID = Integer.parseInt(eventUriForId.getLastPathSegment());
            }
        } else {
            Uri eventUriForId = getContentResolver().insert(eventUri, event);
            eventID = Integer.parseInt(eventUriForId.getLastPathSegment());
        }


        if (isRemind) {

            ContentValues reminderValues = new ContentValues();
            reminderValues.put(CalendarContract.Reminders.EVENT_ID, eventID);
            // Default value of the system. Minutes is a integer

            reminderValues.put(CalendarContract.Reminders.MINUTES, 60);
            // Alert Methods: Default(0), Alert(1), Email(2), SMS(3)
            reminderValues.put(CalendarContract.Reminders.METHOD, 1);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(checkPermission()) {
                    getContentResolver().insert(reminderUri, reminderValues);
                }
            } else {
                getContentResolver().insert(reminderUri, reminderValues);
            }


        }
        return eventID;
    }


    public class EventCalender extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            eventId = addEventToCalender();

            Log.e("eventId", "EventId : "+eventId);
            return eventId;
        }
    }



    private boolean checkPermission(){
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CALENDAR)) {

            } else {

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        MY_PERMISSIONS_REQUEST_READ_CALENDAR);
            }
        }else{
            return true;
        }
        return false;
    }

}
